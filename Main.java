public class Main {

    public static void main(String[] args) {

        CalcString calcObj = new CalcString();
        System.out.println("The result is: " + calcObj.doTheMath("3--2")); // should be 5.0
        CalcString calcObj2 = new CalcString();
        System.out.println("The result is: " + calcObj2.doTheMath("2*(-5)")); // should be -10
        CalcString calcObj3 = new CalcString();
        System.out.println("The result is: " + calcObj3.doTheMath("10*-2+(-2-2-4*-2)/2/-2")); // should be -21.0


    }
}
