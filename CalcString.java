import org.apache.log4j.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by m010587 on 2017-07-20.
 */
public class CalcString implements DoTheMathInterface {

    static Logger log = Logger.getLogger(CalcString.class);

    double numberOne = 0;
    double numberTwo = 0;
    double sum = 0;
    String bString = "";
    String parenthesesString = "";
    int numCount = 0;
    int parenthesesNumCount = 0;

    public double doTheMath(String aString){

        //analyzing input string and making adjustments
        aString = aString.replaceAll("\\s+","").trim();
        aString = aString.replaceAll("[^0-9\\+\\-\\*\\(\\)\\/]", "");
        log.info("aString equals: "+aString);
        String regexPattern = "(?<=\\d)(?=\\D)(?=[^\\.])|(?<=\\D)(?=\\D)|(?<=\\D)(?<=[^-])(?<=[^.])|(?<=\\d\\-)";
        String stringArray[] = aString.split(regexPattern);

        //converting numbers from aString to double
        double cacheNumber;
        for(String s : stringArray){
            try{
                cacheNumber = Double.parseDouble(s);
                bString+=cacheNumber+"";
            }catch (Exception e){
                bString+=s;
            }
        }
        log.info("bString equals: "+bString);

        //checking how many numbers are left
        for(String s: bString.split("")){
            if(s.equals(".")){
                numCount++;
            }
        }

        //real work on the string starts here
        while(bString.contains("(")) {
            Matcher m = Pattern.compile("\\(([^()]+)\\)").matcher(bString);
            while (m.find()) {
                parenthesesString = m.group(1);
            }
            log.info("ParenthesesString: "+parenthesesString);
            for(String s: parenthesesString.split("")){
                if(s.equals(".")){
                    parenthesesNumCount++;
                }
            }

            while (parenthesesString.contains("*") || parenthesesString.contains("/")) {
                log.info("Started FIRST while loop");
                String tempArray[] = parenthesesString.split(regexPattern);
                for (int i = 0; i < tempArray.length; i++) {
                    log.info("Started for loop in first while. Iteration number: " + i);
                    if (tempArray[i].equals("*") || tempArray[i].equals("/")) {
                        parenthesesCleanup(tempArray, i, parenthesesString);
                        break;
                    }
                }
            }
            while ((parenthesesString.contains("+") || parenthesesString.contains("-")) && parenthesesNumCount>=2) {
                log.info("Started SECOND while loop");
                String tempArray[] = parenthesesString.split(regexPattern);
                parenthesesNumCount = 0;
                for(String s :this.parenthesesString.split("")){
                    if(s.equals(".")){
                        parenthesesNumCount++;
                    }
                }
                if(parenthesesNumCount>=2){
                    for (int i = 0; i < tempArray.length; i++) {
                        log.info("Started for loop in second while. Iteration number: " + i);
                        if (tempArray[i].equals("+") || tempArray[i].equals("-")) {
                            parenthesesCleanup(tempArray, i, parenthesesString);
                            break;
                        }
                    }
                }
                else{
                    bString = bString.replace( "("+parenthesesString+")", parenthesesString);
                    break;
                }
            }
            if (parenthesesNumCount==1){
                bString = bString.replace("("+parenthesesString+")",parenthesesString);
            }
        }

        while (bString.contains("*") || bString.contains("/")) {
            log.info("Started THIRD while loop");
            String tempArray[] = bString.split(regexPattern);
            for (int i = 0; i < tempArray.length; i++) {
                log.info("Started for loop in third while. Iteration number: " + i);
                if (tempArray[i].equals("*") || tempArray[i].equals("/")) {
                    twoNumbersInteraction(tempArray,i);
                    break;
                }
            }
        }
        while (bString.contains("+") || bString.contains("-")&&numCount>=2) {
            log.info("Started FOURTH while loop");
            String tempArray[] = bString.split(regexPattern);
            for (int i = 0; i < tempArray.length; i++) {
                log.info("Started for loop in fourth while. Iteration number: " + i);
                if (tempArray[i].equals("+") || tempArray[i].equals("-")) {
                    twoNumbersInteraction(tempArray,i);
                    break;
                }
            }
        }

        return sum;
    }

    //function responsible for clearing up (calculating) parentheses
    public void parenthesesCleanup(String[] tempArray, int i, String parenthesesString){
        this.parenthesesString = parenthesesString;
        numberOne = Double.parseDouble(tempArray[i - 1]);
        numberTwo = Double.parseDouble(tempArray[i + 1]);
        String replacement = checkSymbol(tempArray[i]) + "";
        log.info("Replacement string is: " + replacement);
        this.parenthesesString = parenthesesString.replace(tempArray[i-1]+tempArray[i]+tempArray[i+1], replacement);
        parenthesesNumCount = 0;
        for(String s :this.parenthesesString.split("")){
            if(s.equals(".")){
                parenthesesNumCount++;
            }
        }
        if(parenthesesNumCount>=2){
            bString = bString.replace( tempArray[i-1]+tempArray[i]+tempArray[i+1], replacement);
        }
        else{
            bString = bString.replace( "("+tempArray[i-1]+tempArray[i]+tempArray[i+1]+")", replacement);
        }
        this.parenthesesString="";
        log.info("bString equals: " + bString);
    }

    //function responsible for calculating "normal" equations
    public void twoNumbersInteraction(String[] tempArray, int i){
        numberOne = Double.parseDouble(tempArray[i - 1]);
        numberTwo = Double.parseDouble(tempArray[i + 1]);
        String replacement = checkSymbol(tempArray[i]) + "";
        log.info("Replacement string is: " + replacement);
        bString = bString.replace(numberOne+tempArray[i]+numberTwo, replacement);
        log.info("bString equals: " + bString);
        numCount=0;
        for(String s: bString.split("")){
            if(s.equals(".")){
                numCount++;
            }
        }
    }

    //the +,-,/,* conditions
    public double checkSymbol(String sym){
        if(sym.equals("+")) {
            sum = numberOne + numberTwo;
        }
        if(sym.equals("-")) {
            sum = numberOne - numberTwo;
        }
        if(sym.equals("*")) {
            sum = numberOne * numberTwo;
        }
        if(sym.equals("/")) {
            sum = numberOne / numberTwo;
        }
        return sum;
    }
}
